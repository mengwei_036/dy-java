package com.dyj.applet.domain;

/**
 * @author danmo
 * @date 2024-05-06 13:53
 **/
public class CategoryInfo {

    /**
     *
     * 类目ID
     */
    private Long category_id;
    /**
     * 类目名称
     */
    private String name;
    /**
     * 父类目ID
     */
    private Long parent_id;
    /**
     * 类目层级
     */
    private Integer level;
    /**
     * 是否是叶子结点
     */
    private Boolean is_leaf;
    /**
     * 类目是否开放
     */
    private Boolean  enable;

    public Long getCategory_id() {
        return category_id;
    }

    public void setCategory_id(Long category_id) {
        this.category_id = category_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getParent_id() {
        return parent_id;
    }

    public void setParent_id(Long parent_id) {
        this.parent_id = parent_id;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Boolean getIs_leaf() {
        return is_leaf;
    }

    public void setIs_leaf(Boolean is_leaf) {
        this.is_leaf = is_leaf;
    }

    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }
}
