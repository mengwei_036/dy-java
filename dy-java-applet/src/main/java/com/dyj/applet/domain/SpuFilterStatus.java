package com.dyj.applet.domain;

import java.util.List;

/**
 * @author danmo
 * @date 2024-04-29 16:49
 **/
public class SpuFilterStatus {

    /**
     * 供应商Id
     */
    private String supplier_id;

    /**
     * 状态过滤信息
     */
    private List<SpuFilterStatusInfo> spu_poi_filter_status;

    public String getSupplier_id() {
        return supplier_id;
    }

    public void setSupplier_id(String supplier_id) {
        this.supplier_id = supplier_id;
    }

    public List<SpuFilterStatusInfo> getSpu_poi_filter_status() {
        return spu_poi_filter_status;
    }

    public void setSpu_poi_filter_status(List<SpuFilterStatusInfo> spu_poi_filter_status) {
        this.spu_poi_filter_status = spu_poi_filter_status;
    }
}
