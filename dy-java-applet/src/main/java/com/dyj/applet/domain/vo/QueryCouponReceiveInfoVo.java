package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.CouponReceiveInfo;

import java.util.List;

/**
 * 查询用户可用券信息返回值
 */
public class QueryCouponReceiveInfoVo {

    private List<CouponReceiveInfo> coupon_receive_list;

    public List<CouponReceiveInfo> getCoupon_receive_list() {
        return coupon_receive_list;
    }

    public QueryCouponReceiveInfoVo setCoupon_receive_list(List<CouponReceiveInfo> coupon_receive_list) {
        this.coupon_receive_list = coupon_receive_list;
        return this;
    }
}
