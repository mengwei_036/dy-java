package com.dyj.applet.domain;

/**
 * 查询营销活动返回值
 */
public class QueryPromotionActivityV2 {


    /**
     * <p>营销活动ID</p> 选填
     */
    private String activity_id;
    /**
     * <p style="text-align: left;line-height: 1.38;margin: 0px 0px 0px;"><span style="color: #171A1C;"><span style="font-size: 14px;" elementtiming="element-timing">描述对应营销活动的名称。</span></span></p> 选填
     */
    private String activity_name;
    /**
     * <p>营销活动关联的券模板ID。营销活动进入发放周期后，用户领券行为会消耗该关联券模板ID的库存</p> 选填
     */
    private String coupon_meta_id;
    /**
     * <p><span style="color: #171A1C;"><span style="font-size: 14px;" elementtiming="element-timing">用户可以参与活动（领取小程序券）的次数</span></span></p> 选填
     */
    private Long receive_limit;
    /**
     * <p>营销活动的对应发放场景</p> 选填
     */
    private Integer send_scene;
    /**
     * <p>营销活动状态</p> 选填
     */
    private Integer status;
    /**
     * <p>IM营销活动</p> 选填
     */
    private PromotionActivityImActivity<QueryPromotionActivityImActivityShareFissionConfig> im_activity;
    /**
     * <p>直播间营销活动</p> 选填
     */
    private PromotionActivityLiveActivity live_activity;
    /**
     * <p>侧边栏营销活动</p> 选填
     */
    private PromotionActivitySidebarActivity sidebar_activity;

    public String getActivity_id() {
        return activity_id;
    }

    public QueryPromotionActivityV2 setActivity_id(String activity_id) {
        this.activity_id = activity_id;
        return this;
    }

    public String getActivity_name() {
        return activity_name;
    }

    public QueryPromotionActivityV2 setActivity_name(String activity_name) {
        this.activity_name = activity_name;
        return this;
    }

    public String getCoupon_meta_id() {
        return coupon_meta_id;
    }

    public QueryPromotionActivityV2 setCoupon_meta_id(String coupon_meta_id) {
        this.coupon_meta_id = coupon_meta_id;
        return this;
    }

    public Long getReceive_limit() {
        return receive_limit;
    }

    public QueryPromotionActivityV2 setReceive_limit(Long receive_limit) {
        this.receive_limit = receive_limit;
        return this;
    }

    public Integer getSend_scene() {
        return send_scene;
    }

    public QueryPromotionActivityV2 setSend_scene(Integer send_scene) {
        this.send_scene = send_scene;
        return this;
    }

    public Integer getStatus() {
        return status;
    }

    public QueryPromotionActivityV2 setStatus(Integer status) {
        this.status = status;
        return this;
    }

    public PromotionActivityImActivity<QueryPromotionActivityImActivityShareFissionConfig> getIm_activity() {
        return im_activity;
    }

    public QueryPromotionActivityV2 setIm_activity(PromotionActivityImActivity<QueryPromotionActivityImActivityShareFissionConfig> im_activity) {
        this.im_activity = im_activity;
        return this;
    }

    public PromotionActivityLiveActivity getLive_activity() {
        return live_activity;
    }

    public QueryPromotionActivityV2 setLive_activity(PromotionActivityLiveActivity live_activity) {
        this.live_activity = live_activity;
        return this;
    }

    public PromotionActivitySidebarActivity getSidebar_activity() {
        return sidebar_activity;
    }

    public QueryPromotionActivityV2 setSidebar_activity(PromotionActivitySidebarActivity sidebar_activity) {
        this.sidebar_activity = sidebar_activity;
        return this;
    }


}
