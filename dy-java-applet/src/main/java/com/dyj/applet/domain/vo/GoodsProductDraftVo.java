package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.ProductDraft;

import java.util.List;

/**
 * @author danmo
 * @date 2024-05-06 11:34
 **/
public class GoodsProductDraftVo {

    /**
     * 用于查询下一页
     */
    private String next_cursor;
    /**
     * 是否有下一页
     */
    private Boolean has_more;

    private List<ProductDraft> product_draft_list;

    public String getNext_cursor() {
        return next_cursor;
    }

    public void setNext_cursor(String next_cursor) {
        this.next_cursor = next_cursor;
    }

    public Boolean getHas_more() {
        return has_more;
    }

    public void setHas_more(Boolean has_more) {
        this.has_more = has_more;
    }

    public List<ProductDraft> getProduct_draft_list() {
        return product_draft_list;
    }

    public void setProduct_draft_list(List<ProductDraft> product_draft_list) {
        this.product_draft_list = product_draft_list;
    }
}
