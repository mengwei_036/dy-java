package com.dyj.common.domain;

/**
 * @author danmo
 * @date 2024-05-06 10:54
 **/
public class DyProductBase {
    /**
     * 请求日志ID
     */
    private String log_id;
    /**
     *
     * 网关状态码
     */
    private String gateway_code;
    /**
     * 网关状态信息
     */
    private String gateway_msg;
    /**
     * 业务状态码
     */
    private String biz_code;
    /**
     *
     * 业务状态信息
     */
    private String biz_msg;

    public String getLog_id() {
        return log_id;
    }

    public void setLog_id(String log_id) {
        this.log_id = log_id;
    }

    public String getGateway_code() {
        return gateway_code;
    }

    public void setGateway_code(String gateway_code) {
        this.gateway_code = gateway_code;
    }

    public String getGateway_msg() {
        return gateway_msg;
    }

    public void setGateway_msg(String gateway_msg) {
        this.gateway_msg = gateway_msg;
    }

    public String getBiz_code() {
        return biz_code;
    }

    public void setBiz_code(String biz_code) {
        this.biz_code = biz_code;
    }

    public String getBiz_msg() {
        return biz_msg;
    }

    public void setBiz_msg(String biz_msg) {
        this.biz_msg = biz_msg;
    }
}
